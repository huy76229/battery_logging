#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy

import pandas as pd
from datetime import datetime
import os

from dbot_msgs.msg import RobotStatus, RobotState

# robot state
IDLE = "IDLE"
A2B = "AtoB"
A2H = "AtoH"
A2D = "AtoH"
A2S = "AtoS"
PATROL = "PATROL"
HOLD = "HOLD"
DOCKED = "Docked"
DOCKING = "Docking"
UNDOCKING = "Undocking"

# check time(unit: second)
CHECK_BAT_EVERY = 10 * 60
# CHECK_BAT_EVERY = 30

root_dir = rospy.get_param('dogu_dir')


class RobotCommunication:
    def __init__(self):

        rospy.init_node('battery_logging_node', anonymous=True)

        self.battery1_value = "0"
        self.battery2_value = "0"
        self.robot_state = IDLE

        self.pub_robot_state = rospy.Publisher('/robot_state', RobotState, queue_size=20)
        self.set_state()

        self.subscribe_robot()

    def set_state(self, state=IDLE):
        """
        set state
        """

        # publish state
        robot_state_ = RobotState()
        robot_state_.robot_state = state
        self.pub_robot_state.publish(robot_state_)

        print("[STATE] {}".format(state))

    def robot_state_callback(self, msg):
        self.robot_state = msg.robot_state

    def subscribe_robot_state(self):
        """
        subscribe robot state
        """
        rospy.Subscriber("/robot_state", RobotState, self.robot_state_callback)

    def robot_status_callback(self, msg):
        self.battery1_value = str(msg.battery)
        self.battery2_value = str(msg.battery2)

    def subscribe_robot_status(self):
        """
        subscribe robot status
        """
        rospy.Subscriber("/robot_status", RobotStatus, self.robot_status_callback)

    def subscribe_robot(self):
        """
        subscribe robot
        """
        self.subscribe_robot_status()
        self.subscribe_robot_state()

    def get_battery1_level(self):
        return self.battery1_value

    def get_battery2_level(self):
        return self.battery2_value

    def get_robot_state(self):
        return self.robot_state


def get_current_time():
    # datetime object containing current date and time
    now = datetime.now()

    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

    return dt_string


def create_file_name():
    # get current time
    current_time = datetime.now()

    time_ = "%d%d%d_%d%d%d" % (current_time.year, current_time.month, current_time.day,
                               current_time.hour, current_time.minute, current_time.second)

    return time_


def save_logging_data(filename, data1, data2):

    data_ = pd.DataFrame([[data1, data2]], columns=["Bat1", "Bat2"], index=[get_current_time()])

    if not os.path.exists(filename):
        data_.to_csv(filename, index=True, mode='w', encoding='utf-8-sig')

    else:
        data_.to_csv(filename, index=True, mode='a', encoding='utf-8-sig', header=False)


if __name__ == "__main__":
    
    robot_ros = RobotCommunication()

    if not os.path.exists(root_dir):
        os.makedirs(root_dir)

    create_logging_file_name_ = "{}/{}.csv".format(root_dir, create_file_name())

    # # logging every 1 minute
    # rate = rospy.Rate(1)
    # while not rospy.is_shutdown():
    #     bat1 = robot_ros.get_battery1_level()
    #     bat2 = robot_ros.get_battery2_level()
    #
    #     save_logging_data(create_logging_file_name_, int(bat1), int(bat2))
    #
    #     print("Bat 1: ", bat1)
    #     print("Bat 2: ", bat2)
    #
    #     rate.sleep()

    bat1 = robot_ros.get_battery1_level()
    bat2 = robot_ros.get_battery2_level()
    print("-----------------------------")
    print("Bat 1: ", bat1)
    print("Bat 2: ", bat2)
    print("-----------------------------")

    is_undocking = True
    is_in_charging = False

    check_time = datetime.now()

    # logging every 10 minute
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        bat1 = robot_ros.get_battery1_level()
        bat2 = robot_ros.get_battery2_level()

        if robot_ros.get_robot_state() == IDLE and is_undocking and int(bat1) > 0:
            bat1 = robot_ros.get_battery1_level()
            bat2 = robot_ros.get_battery2_level()

            save_logging_data(create_logging_file_name_, int(bat1), int(bat2))

            robot_ros.set_state(DOCKING)
            is_undocking = False
            is_in_charging = False

            print("-----------------------------")
            print("Bat 1: ", bat1)
            print("Bat 2: ", bat2)
            print("-----------------------------")
            print("DOCKING.............")

        if robot_ros.get_robot_state() == UNDOCKING:
            is_undocking = True
            is_in_charging = False

        if robot_ros.get_robot_state() == DOCKED:
            if not is_in_charging:
                print("DOCKED.............")
                check_time = datetime.now()

            is_in_charging = True

        if (datetime.now() - check_time).total_seconds() >= CHECK_BAT_EVERY and is_in_charging:
            robot_ros.set_state(UNDOCKING)
            check_time = datetime.now()

            print("UNDOCKING.............")

        rate.sleep()
